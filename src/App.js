import React from 'react'
import { connect } from 'react-redux'
import { actions } from './redux'

const { addButton, removeButton, clearButtons } = actions

const App = ({ activeButtons, output, addButton, removeButton, clearButtons }) => {
  const clickHanlder = button => 
    !activeButtons.includes(button) ? addButton(button) : removeButton(button)
  return (
    <section className="main">
      <div className="buttons">
        <button 
          className={(activeButtons.includes('day') ? 'pressed' : '')} 
          onClick={() => clickHanlder('day')}
        >
          Day
        </button>
        <button 
          className={(activeButtons.includes('month') ? 'pressed' : '')}
          onClick={() => clickHanlder('month')}
        >
          Month
        </button>
        <button 
          className={(activeButtons.includes('year') ? 'pressed' : '')}
          onClick={() => clickHanlder('year')}
        >
          Year
        </button>
      </div>
      <p className="date">{output}</p>
      <button onClick={clearButtons}>Clear</button>
    </section>
  )
}

const mapStateToProps = state => ({
  activeButtons: state.activeButtons,
  output: state.output
})

const mapDispatchToProps = dispatch => ({
  addButton: button => dispatch(addButton(button)),
  removeButton: button => dispatch(removeButton(button)),
  clearButtons: () => dispatch(clearButtons()),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
