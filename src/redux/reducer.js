const date = new Date()

const initialState = {
    activeButtons: [],
    date: {
        day: date.getDate(),
        month: date.getMonth() + 1,
        year: date.getFullYear()
    },
    output: ''
}

const setOutput = (button, index, date) => {
    const additionToOutput = parseInt(date[button]) > 9 ?
        date[button] :
        '0' + date[button]
    return index === 0 ?
        additionToOutput :
        '.' + additionToOutput
  }

const reducer = (state = initialState, action) => {
    let newActiveButtons, newOutput
    switch(action.type) {
        case 'ADD_BUTTON':
            newActiveButtons = [...state.activeButtons, action.payload].sort()
            newOutput = ''
            newActiveButtons.map((button, index) => newOutput += setOutput(button, index, state.date))
            return {
                ...state,
                activeButtons: newActiveButtons,
                output: newOutput
            }
        case 'REMOVE_BUTTON':
            newActiveButtons = state.activeButtons.filter(button => button !== action.payload)
            newOutput = ''
            newActiveButtons.map((button, index) => newOutput += setOutput(button, index, state.date))
            return { 
                ...state,
                activeButtons: newActiveButtons,
                output: newOutput
            }
        case 'CLEAR_BUTTONS':
            return { ...state, activeButtons: [], output: '' }
        default:
            return state
    }
}

export default reducer