export const addButton = button => ({
    type: 'ADD_BUTTON',
    payload: button
})

export const removeButton = button => ({
    type: 'REMOVE_BUTTON',
    payload: button
})

export const clearButtons = () => ({
    type: 'CLEAR_BUTTONS'
})